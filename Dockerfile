FROM node:14.15.4

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm ci
COPY . .
RUN npm run test
