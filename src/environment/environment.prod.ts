import { IEnvironment } from "./environment.interface";

export const EnvironmentProd: IEnvironment = {
  pathToCities: __dirname + "/../../assets/in/cities.txt",
  pathToGson: __dirname + "/../../assets/out/",
};
