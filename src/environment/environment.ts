import { IEnvironment } from "./environment.interface";
import { EnvironmentTest } from "./environment.test";
import { EnvironmentProd } from "./environment.prod";
const currentEnv = process.env.NODE_ENV || "prod";
console.log("Environment is:", currentEnv);

export const environment: IEnvironment =
  currentEnv === "prod" ? { ...EnvironmentProd } : { ...EnvironmentTest };
