export interface IEnvironment {
  pathToCities: string;
  pathToGson: string;
}
