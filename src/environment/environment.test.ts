import { IEnvironment } from "./environment.interface";

export const EnvironmentTest: IEnvironment = {
  pathToCities: __dirname + "/../../assets/in/cities.txt",
  pathToGson: __dirname + "/../test/out/",
};
