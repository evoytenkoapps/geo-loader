import * as assert from "assert";
import axios from "axios";
import sinon from "sinon";
import { GeoLoaderService } from "./geo-loader";
import { diContainer } from "../../di/inversify.config";
import { TYPES } from "../../di/di-types";
import { OsmMock } from "../../test/mock/osm.mock";
import { GsonMock } from "../../test/mock/gson.mock";

describe("GeoLoaderService", function () {
  it("Get gson for each city", async () => {
    const service = diContainer.get<GeoLoaderService>(TYPES.GeoLoaderService);
    const osmMock = new OsmMock();
    const gsonMock = new GsonMock();
    const cities = ["Омск", "Москва"];
    const getOsm = () => Promise.resolve(osmMock);
    const getGson = () => Promise.resolve(gsonMock);
    const aStub = sinon.stub(axios, "get");
    aStub
      .withArgs(
        encodeURI(
          `https://nominatim.openstreetmap.org/search.php?q=${cities[0]}&polygon_geojson=1&format=jsonv2`
        )
      )
      .callsFake(getOsm)
      .withArgs(
        encodeURI(
          `https://nominatim.openstreetmap.org/search.php?q=${cities[1]}&polygon_geojson=1&format=jsonv2`
        )
      )
      .callsFake(getOsm)
      .withArgs(
        encodeURI(
          `http://polygons.openstreetmap.fr/get_geojson.py?id=${osmMock.data[0].osm_id}&params=0`
        )
      )
      .callsFake(getGson);

    const result = await service.loadGsons(cities);
    assert.deepStrictEqual(result.length === 2, true);
    assert.deepStrictEqual(result[0].gson.type === gsonMock.data.type, true);
    assert.deepStrictEqual(result[1].gson.type === gsonMock.data.type, true);
  });
});
