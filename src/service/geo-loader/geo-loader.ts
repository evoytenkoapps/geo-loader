import { IGeoGson, IGeoLoaderService, IOsmInfo } from "./geo-loader.interfaces";
import { inject, injectable } from "inversify";
import axios from "axios";
import { ILogger } from "../../common/logger/logger.interface";
import { TYPES } from "../../di/di-types";

@injectable()
export class GeoLoaderService implements IGeoLoaderService {
  constructor(@inject(TYPES.Logger) private readonly logger: ILogger) {}

  public async loadGsons(cities: string[]): Promise<IGeoGson[]> {
    const osms: IOsmInfo[] = await this.getOsms(cities);
    return await this.getGsons(osms);
  }

  private async getOsms(cities: string[]): Promise<IOsmInfo[]> {
    const result: IOsmInfo[] = [];
    for (const city of cities) {
      try {
        this.logger.log("Get OSM for:", city);
        const url = encodeURI(
          `https://nominatim.openstreetmap.org/search.php?q=${city}&polygon_geojson=1&format=jsonv2`
        );
        const res = await axios.get(url, {
          responseType: "json",
        });
        const osm = res.data[0].osm_id;
        if (osm) {
          this.logger.log("Got OSM:", osm, "for", city);
          result.push({ city, osm });
        }
      } catch (e) {
        this.logger.error(e, "Cant get OSM for:", city);
      }
    }
    this.logger.log("Got total ", result.length, "osms");
    return result;
  }

  private async getGsons(osms: IOsmInfo[]): Promise<IGeoGson[]> {
    const result: IGeoGson[] = [];
    for (const info of osms) {
      try {
        this.logger.log("Get GeoJson for:", info.city);
        const url = encodeURI(
          `http://polygons.openstreetmap.fr/get_geojson.py?id=${info.osm}&params=0`
        );
        const res = await axios.get(url, {
          responseType: "json",
        });
        const gson = res.data;
        if (gson) {
          this.logger.log("Got GeoJson for", info.city);
          result.push({ city: info.city, gson });
        }
      } catch (e) {
        this.logger.error(e, "Cant get GeoJson for:", info.city);
      }
    }
    this.logger.log("Got total ", result.length, "gsons");
    return result;
  }
}
