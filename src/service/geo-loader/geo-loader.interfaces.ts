export interface IGeoLoaderService {
  loadGsons(cities: string[]): Promise<IGeoGson[]>;
}

export interface IOsmInfo {
  osm: number;
  city: string;
}

export interface IGeoGson {
  city: string;
  gson: any;
}
