import { IRepository } from "./repository.interface";
import { inject, injectable } from "inversify";
import { TYPES } from "../../di/di-types";
import { FileReader } from "../file-reader/file-reader";
import { IGeoGson } from "../geo-loader/geo-loader.interfaces";
import { environment } from "../../environment/environment";
import { ILogger } from "../../common/logger/logger.interface";

@injectable()
export class Repository implements IRepository {
  constructor(
    @inject(TYPES.FileReader)
    private readonly fileReaderService: FileReader,
    @inject(TYPES.Logger)
    private readonly logger: ILogger
  ) {}

  public async getCities(): Promise<string[]> {
    return this.fileReaderService.read();
  }

  public async saveGsons(gsons: IGeoGson[]): Promise<void> {
    for (const gson of gsons) {
      await this.fileReaderService.write(
        gson,
        environment.pathToGson + "/" + gson.city + ".json"
      );
    }
    this.logger.log("Totally was saved: ", gsons.length, "files");
  }
}
