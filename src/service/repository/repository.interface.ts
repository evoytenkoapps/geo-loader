import {IGeoGson} from "../geo-loader/geo-loader.interfaces";

export interface IRepository {
  getCities(): Promise<string[]>;
  saveGsons(gsons: IGeoGson[]): Promise<void>;
}
