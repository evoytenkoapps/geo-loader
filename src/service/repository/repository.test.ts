import { diContainer } from "../../di/inversify.config";
import { TYPES } from "../../di/di-types";
import { IRepository } from "./repository.interface";
import { IGeoGson } from "../geo-loader/geo-loader.interfaces";
import fs from "fs";
import path from "path";
import assert from "assert";
import { environment } from "../../environment/environment";

describe("Repository", function () {
  it("Files has city name", async () => {
    const service = diContainer.get<IRepository>(TYPES.Repository);
    const data: IGeoGson[] = [
      { gson: "asdasd", city: "Москва" },
      { gson: "asdasd", city: "Омск" },
    ];
    await service.saveGsons(data);

    const buffer: any = await fs.promises.readFile(
      path.resolve(
        path.resolve(environment.pathToGson + "/" + data[0].city + ".json")
      ),
      "utf8"
    );

    const readedData: IGeoGson = JSON.parse(buffer);

    assert.deepStrictEqual(data[0].city === readedData.city, true);
  });
});
