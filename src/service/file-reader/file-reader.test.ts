import { diContainer } from "../../di/inversify.config";
import { TYPES } from "../../di/di-types";
import { FileReader } from "./file-reader";
import * as assert from "assert";
import fs from "fs";
import { environment } from "../../environment/environment";

describe("FileReader", function () {
  it("Save data", async () => {
    const reader = diContainer.get<FileReader>(TYPES.FileReader);

    const writeData = [{ a: 1 }, { b: 2 }];
    const pathToFile = environment.pathToGson + "test.txt";

    await reader.write(writeData, pathToFile);
    const buffer: any = await fs.promises.readFile(pathToFile, "utf8");

    const readedData = JSON.parse(buffer);

    assert.deepStrictEqual(writeData[0].a === readedData[0].a, true);
  });
});
