import { inject, injectable } from "inversify";
import { TYPES } from "../../di/di-types";
import { ILogger } from "../../common/logger/logger.interface";
import * as fs from "fs";
import * as path from "path";
import { environment } from "../../environment/environment";

@injectable()
export class FileReader {
  constructor(@inject(TYPES.Logger) private readonly logger: ILogger) {}

  public async read(): Promise<string[]> {
    const pathToFile = environment.pathToCities;
    this.logger.log("Read file", pathToFile);
    const buffer = await fs.promises.readFile(path.resolve(pathToFile), "utf8");
    const file = buffer.split(/\r\n|\n/).filter((el) => el !== "");
    this.logger.log("Loaded file length", file.length);
    return file;
  }

  public async write(data: any, pathToFile: string): Promise<void> {
    this.logger.log("Writing data to", pathToFile);
    try {
      await fs.promises.writeFile(pathToFile, JSON.stringify(data), "utf8");
      this.logger.log("File: ", pathToFile, " was saved");
    } catch (e) {
      this.logger.error(e, "Cant write file");
      throw e;
    }
  }
}
