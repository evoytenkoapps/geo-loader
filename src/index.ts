import { diContainer } from "./di/inversify.config";
import { ILogger } from "./common/logger/logger.interface";
import { TYPES } from "./di/di-types";
import { Server } from "./server";

const logger = diContainer.get<ILogger>(TYPES.Logger);
const server = diContainer.get<Server>(TYPES.Server);

logger.log("Server started");
logger.log("Environment is", process.env.NODE_ENV + "");

server.getData();



process.on("unhandledRejection", (reason: string, p: Promise<any>) => {
  logger.error("unhandledRejection", reason);
  throw reason;
});

process.on("uncaughtException", (error: Error) => {
  logger.error("uncaughtException", error);
  process.exit(1);
});
