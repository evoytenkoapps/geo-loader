import "reflect-metadata";

import {Container} from "inversify";
import {TYPES} from "./di-types";
import {PinoLogger} from "../common/logger/pino-logger";
import {ILogger} from "../common/logger/logger.interface";
import {FileReader} from "../service/file-reader/file-reader";
import {Server} from "../server";
import {GeoLoaderService} from "../service/geo-loader/geo-loader";
import {Repository} from "../service/repository/repository";

export const diContainer = new Container();

diContainer.bind<ILogger>(TYPES.Logger).to(PinoLogger).inSingletonScope();
diContainer
  .bind<FileReader>(TYPES.FileReader)
  .to(FileReader)
  .inSingletonScope();

diContainer.bind(TYPES.Server).to(Server).inSingletonScope();

diContainer
  .bind(TYPES.GeoLoaderService)
  .to(GeoLoaderService)
  .inSingletonScope();

diContainer.bind(TYPES.Repository).to(Repository);
